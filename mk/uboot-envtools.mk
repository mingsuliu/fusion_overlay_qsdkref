define uboot-envtools_overlay_install
	
	if [ -d $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME) ]; then \
		$(CP) -a $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/* $(1)/; \
	fi

endef

Package/uboot-envtools/install += $(uboot-envtools_overlay_install)
