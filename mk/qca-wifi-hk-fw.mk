PKG_SOURCE_v2=qca-wifi-fw-QCA8074_v2.0-$(PKG_VERSION).tar.bz2

define BuildCustomWifiFirmware
	$(INSTALL_DIR) $(PKG_BUILD_DIR)/wififw/fwtemp \
		$(PKG_BUILD_DIR)/wififw/staging_dir
	$(INSTALL_DIR) $(PKG_BUILD_DIR)/wififw/fwtemp \
		$(PKG_BUILD_DIR)/wififw/staging_dir
	$(TAR) xvf $(TOPDIR)/dl/$(PKG_SOURCE_v2) --strip-components 1 \
		-C $(PKG_BUILD_DIR)/wififw/fwtemp
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/wififw/fwtemp/PIL_IMAGES/* \
		$(PKG_BUILD_DIR)/wififw/staging_dir
	$(INSTALL_DATA) $(PKG_BUILD_DIR)/wififw/fwtemp/bdwlan* \
		$(PKG_BUILD_DIR)/wififw/staging_dir
	$(INSTALL_DATA) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/bdf/$(CONFIG_ZYUMAC_MODEL_NAME)/bdwlan.bin \
		$(PKG_BUILD_DIR)/wififw/staging_dir/bdwlan.bin
	$(INSTALL_DATA) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/firmware_rdp_feature.ini \
		$(PKG_BUILD_DIR)/wififw/staging_dir/
	mksquashfs $(PKG_BUILD_DIR)/wififw/staging_dir \
		$(PKG_BUILD_DIR)/wififw/wifi_fw.squashfs \
		-nopad -noappend -root-owned -comp xz -b 256k
	dd if=$(PKG_BUILD_DIR)/wififw/wifi_fw.squashfs \
		of=$(TOPDIR)/../images/$(CONFIG_ZYUMAC_MODEL_NAME)/wlan_proc/build/ms/bin/8074.wlanfw.eval_v2/FW_IMAGES/wifi_fw_squashfs_v2.img \
		bs=2k conv=sync
	$(INSTALL_DATA) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/ubi-wifi.cfg \
		$(PKG_BUILD_DIR)/wififw/ubi-wifi.cfg
	cd $(PKG_BUILD_DIR)/wififw && ubinize -m 2048 -p 128KiB \
		-o $(PKG_BUILD_DIR)/wififw/wifi_fw.ubi \
		$(PKG_BUILD_DIR)/wififw/ubi-wifi.cfg
	dd if=$(PKG_BUILD_DIR)/wififw/wifi_fw.ubi \
		of=$(TOPDIR)/../images/$(CONFIG_ZYUMAC_MODEL_NAME)/wlan_proc/build/ms/bin/8074.wlanfw.eval_v2/FW_IMAGES/wifi_fw_ubi_v2.img \
		bs=2k conv=sync
endef

Package/$(PKG_NAME)-hw1-10.4-asic/install += $(BuildCustomWifiFirmware)
