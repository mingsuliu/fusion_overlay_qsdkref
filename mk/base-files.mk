define base-files_overlay_install
	;\
	echo "Install Zyxel base-files"
ifneq ($(CONFIG_ZYUMAC_MODEL_NAME),)
	echo "DEVICE_ZYUMAC_MODEL_NAME='$(CONFIG_ZYUMAC_MODEL_NAME)'" >>$(1)/etc/device_info
endif

ifneq ($(CONFIG_ZYUMAC_BOARD_NAME),)
	echo "DEVICE_ZYUMAC_BOARD_NAME='$(CONFIG_ZYUMAC_BOARD_NAME)'" >>$(1)/etc/device_info
endif
	# Copy files for generic
	if [ -d $(TOPDIR)/overlay/generic/$(PKG_NAME)/files/. ]; then \
		$(CP) $(TOPDIR)/overlay/generic/$(PKG_NAME)/files/* $(1)/; \
		if [ -d $(TOPDIR)/overlay/generic/$(PKG_NAME)/files/etc/uci-defaults ]; then \
			$(INSTALL_BIN) $(TOPDIR)/overlay/generic/$(PKG_NAME)/files/etc/uci-defaults/* $(1)/etc/uci-defaults/; \
		fi \
	fi

	# Copy files for product line overwriting
	if [ -d $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME) ]; then \
                $(CP) -a $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/files/* $(1)/; \
	fi

	# Copy files for board profile
	if [ -d $(PLATFORM_DIR)/base-files-$(PROFILE)/. ]; then \
		$(CP) $(PLATFORM_DIR)/base-files-$(PROFILE)/* $(1)/; \
	fi

endef

Package/base-files/install += $(base-files_overlay_install)
