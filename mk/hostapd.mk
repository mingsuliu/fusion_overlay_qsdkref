define hostapd_overlay_patch
	
	if [ -d $(TOPDIR)/overlay/generic/$(PKG_NAME) ]; then \
		$(CP) $(TOPDIR)/overlay/generic/$(PKG_NAME)/patches/* $(TOPDIR)/package/network/services/$(PKG_NAME)/patches/; \
	fi

endef

Build/Prepare = $(hostapd_overlay_patch)
Build/Prepare += $(Build/Prepare/Default)

define hostapd_overlay_install

	if [ -d $(TOPDIR)/overlay/generic/$(PKG_NAME) ]; then \
		$(INSTALL_DIR) $(1)/lib/wifi ;\
		$(INSTALL_BIN) $(TOPDIR)/overlay/generic/$(PKG_NAME)/files/lib/wifi/hostapd.sh $(1)/lib/wifi ;\
	fi

endef
Package/hostapd-common-old/install += $(hostapd_overlay_install)
