spf:= 9.0 10.0 11.0
Hooks/Prepare/Pre += fusion_uboot_install_patches
define fusion_uboot_install_patches

	if [ ! -d patches ]; then \
		mkdir patches; \
	fi

	$(foreach rev,$(spf), \
		$(if $(findstring y,$(CONFIG_PACKAGE_fuMAC_$(rev))), \
			if [ -d $(TOPDIR)/overlay/ipq/u-boot-2016/SPF$(rev)/$(CONFIG_FUSION_MODEL_NAME)/patches/ ]; then \
				$(CP) $(TOPDIR)/overlay/ipq/u-boot-2016/SPF$(rev)/$(CONFIG_FUSION_MODEL_NAME)/patches/* patches/; \
			fi; \
			if [ -d $(TOPDIR)/overlay/ipq/u-boot-2016/SPF$(rev)/patches/ ]; then \
				$(CP) $(TOPDIR)/overlay/ipq/u-boot-2016/SPF$(rev)/patches/* patches/; \
			fi \
		) \
	)
endef

