
define busybox_patch

	$(CP) $(TOPDIR)/overlay/generic/$(PKG_NAME)/patches/* $(TOPDIR)/package/utils/$(PKG_NAME)/patches/; \
	if [ -d $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME) ]; then \
		$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/patches/* $(TOPDIR)/package/utils/$(PKG_NAME)/patches/; \
	fi;
endef

Build/Prepare = $(busybox_patch)
Build/Prepare += $(Build/Prepare/Default)

