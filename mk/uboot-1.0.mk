PKG_BUILD_DIR_BK:=$(PKG_BUILD_DIR)backup

define uboot-ipq40xx_overlay_install

	$(CP) $(PKG_BUILD_DIR_BK)/u-boot $(BIN_DIR)/openwrt-ipq40xx-u-bootb.elf
	$(CP) $(PKG_BUILD_DIR_BK)/u-boot $(BIN_DIR)/openwrt-ipq40xx-u-bootb-stripped.elf
	$(STRIP) $(BIN_DIR)/openwrt-ipq40xx-u-bootb-stripped.elf
	$(CP) $(PKG_BUILD_DIR_BK)/u-boot.bin $(BIN_DIR)/openwrt-ipq40xx-u-bootb.img

endef

spf:= 6.1 9.0
Hooks/Prepare/Pre += fusion_uboot-ipq40xx_install_patches
define fusion_uboot-ipq40xx_install_patches
	if [ ! -d patches ]; then \
		mkdir patches; \
	fi

	$(foreach rev,$(spf), \
		$(if $(findstring y,$(CONFIG_PACKAGE_fuUboot-ipq40xx_$(rev))), \
			$(CP) $(TOPDIR)/nucleus/src/fuUboot-ipq40xx/SPF$(rev)/patches/* patches/; \
		) \
	)
endef

define uboot-ipq40xx_overlay_compile

	VERSION_CODE='$(VERSION_NICK) $(VERSION_NUMBER)' \
	REVISION='$(REVISION)' \
	$(MAKE) -C $(PKG_BUILD_DIR_BK) HOSTCC=$(TARGET_CC) \
	HOSTSTRIP=true CROSS_COMPILE=$(TARGET_CROSS) \
	QSDK_CFLAGS="$(TARGET_CFLAGS) -fpie" \
	QSDK_LDFLAGS="$(TARGET_LDFLAGS) -pie" \
	SKS_UBOOT_BK=1

endef

define uboot-ipq40xx_overlay_config

	$(CP) -a $(PKG_BUILD_DIR) $(PKG_BUILD_DIR_BK) ;\

endef

define uboot-ipq40xx_overlay_clean

	$(call Clean,default)
	$(RM) -rf $(PKG_BUILD_DIR_BK)

endef

Package/uboot-ipq40xx/install += $(uboot-ipq40xx_overlay_install)
Build/Compile += $(uboot-ipq40xx_overlay_compile)
Build/Clean = $(uboot-ipq40xx_overlay_clean)
Build/Configure += $(uboot-ipq40xx_overlay_config)
