Hooks/Prepare/Pre += qca-ssdk_install_patches
define qca-ssdk_install_patches
    
	if [ ! -d patches ]; then \
		mkdir patches; \
	fi

	if [ -d $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/patches/ ]; then \
		$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/patches/* patches/; \
	fi

endef

