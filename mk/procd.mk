define procd_overlay_patch
	
	if [ ! -d $(TOPDIR)/package/system/$(PKG_NAME)/patches ]; then \
		mkdir $(TOPDIR)/package/system/$(PKG_NAME)/patches; \
	fi
	$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/patches/* $(TOPDIR)/package/system/$(PKG_NAME)/patches/; \

endef

Build/Prepare = $(procd_overlay_patch)
Build/Prepare += $(Build/Prepare/Default)
