define qca-cnss-daemon_overlay_patch
	
	if [ ! -d $(TOPDIR)/qca/feeds/qca/utils/$(PKG_NAME)/patches ]; then \
		mkdir $(TOPDIR)/qca/feeds/qca/utils/$(PKG_NAME)/patches; \
	fi
	$(CP) $(TOPDIR)/overlay/$(BOARD)/$(PKG_NAME)/patches/* $(TOPDIR)/qca/feeds/qca/utils/$(PKG_NAME)/patches/;
endef

Build/Prepare = $(qca-cnss-daemon_overlay_patch)
Build/Prepare += $(Build/Prepare/Default)
